package com.secuera.saml2auth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Saml2OauthApplication.class)
public class Saml2OauthAppTest {
    @Test
    public void contextLoads() {}
}
