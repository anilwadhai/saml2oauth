package com.secuera.saml2auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.secuera.saml2auth.model.AuthTokenRequest;
import com.secuera.saml2auth.service.Saml2AuthService;

@RestController
@RequestMapping("/oauth2/token")
public class Saml2OauthController {

    @Autowired
    private Saml2AuthService saml2AuthService;

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<OAuth2AccessToken> oauthToken(@RequestBody @Validated AuthTokenRequest request) throws Exception {
        OAuth2AccessToken token = saml2AuthService.getToken(request);
        return new ResponseEntity<OAuth2AccessToken>(token, HttpStatus.OK);
    }

}
