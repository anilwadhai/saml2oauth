package com.secuera.saml2auth.model;

import javax.validation.constraints.NotNull;

public class AuthTokenRequest {

    public AuthTokenRequest() {
    }

    @NotNull(message = "Assertion must be present!")
    private String assertion;

    public String getAssertion() {
        return assertion;
    }

    public void setAssertion(String assertion) {
        this.assertion = assertion;
    }

}
