package com.secuera.saml2auth.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

@Component
public class Saml2OauthUtil {


    @Autowired
    private ResourceLoader resourceLoader;

    public String readFileToString(String fileName) throws IOException {
        File file = resourceLoader.getResource("classpath:" + fileName).getFile();
        StringBuilder fileData = new StringBuilder(1000);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }

        reader.close();
        String returnStr = fileData.toString().trim().replace("\n", "").replace("\r", "");
        return returnStr;

    }
}
