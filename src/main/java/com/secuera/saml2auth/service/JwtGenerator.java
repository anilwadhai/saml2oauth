package com.secuera.saml2auth.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Component;

@Component
public class JwtGenerator {

    @Autowired
    private DefaultTokenServices tokenServices;


    private Set<String> getScope() {
        Set<String> scopes = new HashSet<String>();
        scopes.add("read");
        scopes.add("write");
        return scopes;
    }

    public OAuth2AccessToken getAccessToken(String uid, Set<GrantedAuthority> authorities) {
        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        // authorizationParameters.put("grant", "authorization_code");
        OAuth2Request authorizationRequest =
                new OAuth2Request(authorizationParameters, null, authorities, true, getScope(), null, "", null, null);
        User userPrincipal = new User(uid, "", true, true, true, true, authorities);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities);
        OAuth2Authentication authentication = new OAuth2Authentication(authorizationRequest, authenticationToken);
        authentication.setAuthenticated(true);
        OAuth2AccessToken accessToken = tokenServices.createAccessToken(authentication);
        return accessToken;
    }
}
