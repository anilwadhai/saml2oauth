package com.secuera.saml2auth.service;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import com.secuera.saml2auth.model.AuthTokenRequest;

public interface Saml2AuthService {

    public OAuth2AccessToken getToken(AuthTokenRequest request) throws Exception;

}
