package com.secuera.saml2auth.service;

import java.io.StringReader;
import java.util.List;
import javax.annotation.PostConstruct;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Response;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.secuera.saml2auth.util.Saml2OauthUtil;


@Component
public class SamlAssertionValidator {

    Logger log = LoggerFactory.getLogger(SamlAssertionValidator.class);

    private String idpPublicKey;

    @Value("${idp.public.key.path}")
    private String idpPublicKeyPath;

    @Autowired
    private Saml2OauthUtil saml2OauthUtil;

    @PostConstruct
    public void init() {

        try {
            DefaultBootstrap.bootstrap();
            idpPublicKey = saml2OauthUtil.readFileToString(idpPublicKeyPath);

        } catch (Exception e) {
            log.error("Unable to init saml " + e.getMessage());
        }
    }


    public List<Assertion> getAssertions(String SAMLResponse) throws Exception {
        Response response = (Response) unmarshall(SAMLResponse);
        List<Assertion> list = response.getAssertions();
        return list;
    }

    private XMLObject unmarshall(String samlResponse) throws Exception {
        BasicParserPool parser = new BasicParserPool();
        parser.setNamespaceAware(true);
        StringReader reader = new StringReader(samlResponse);
        Document doc = parser.parse(reader);
        Element samlElement = doc.getDocumentElement();
        UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
        Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(samlElement);
        if (unmarshaller == null) {
            throw new Exception("Failed to unmarshal");
        }

        return unmarshaller.unmarshall(samlElement);
    }

    public boolean validAssertion(Assertion assertion) throws ValidationException {
        Signature signature = assertion.getSignature();
        String responsePublicKey = signature.getKeyInfo().getX509Datas().get(0).getX509Certificates().get(0).getValue();
        responsePublicKey = responsePublicKey.replace("\n", "");
        if (!responsePublicKey.equals(idpPublicKey)) {
            throw new ValidationException("The public key does not match !");
        }
        SAMLSignatureProfileValidator validator = new SAMLSignatureProfileValidator();
        validator.validate(signature);
        return true;
    }



}
