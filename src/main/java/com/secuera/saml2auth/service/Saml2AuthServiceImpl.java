package com.secuera.saml2auth.service;

import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSAnyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import com.secuera.saml2auth.model.AuthException;
import com.secuera.saml2auth.model.AuthTokenRequest;

@Service
public class Saml2AuthServiceImpl implements Saml2AuthService {

    private Logger log = LoggerFactory.getLogger(Saml2AuthServiceImpl.class);

    @Autowired
    private SamlAssertionValidator assertionValidator;

    @Autowired
    private JwtGenerator jwtGenerator;




    public OAuth2AccessToken getToken(AuthTokenRequest request) {
        OAuth2AccessToken token = null;
        try {
            byte[] base64DecodedResponse = Base64.getDecoder().decode(request.getAssertion());
            List<Assertion> assertions = assertionValidator.getAssertions(new String(base64DecodedResponse));
            Assertion assertion = null;
            if (assertions != null && assertions.size() > 0) {
                assertion = assertions.get(0);
                try {
                    assertionValidator.validAssertion(assertion);
                    Attribute attribute = assertions.get(0).getAttributeStatements().get(0).getAttributes().get(0);
                    String value = getAttributeValue(attribute.getAttributeValues().get(0));
                    token = jwtGenerator.getAccessToken(value, new HashSet<GrantedAuthority>());
                } catch (Exception e) {
                    log.warn("Invalid signarure : " + e.getMessage());
                    throw new AuthException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),
                            "Invalid signarure");
                }
            } else {
                log.error("Assersion not found");
                throw new AuthException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), "Assersion not found");
            }



        } catch (Exception e) {
            log.error("Failed to get access token : " + e.getMessage());
            throw new AuthException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), e.getMessage());
        }

        return token;
    }

    private String getAttributeValue(XMLObject attributeValue) {
        return attributeValue == null ? null
                : attributeValue instanceof XSString ? getStringAttributeValue((XSString) attributeValue)
                        : attributeValue instanceof XSAnyImpl ? getAnyAttributeValue((XSAnyImpl) attributeValue)
                                : attributeValue.toString();
    }

    private String getStringAttributeValue(XSString attributeValue) {
        return attributeValue.getValue();
    }

    private String getAnyAttributeValue(XSAnyImpl attributeValue) {
        return attributeValue.getTextContent();
    }
}
