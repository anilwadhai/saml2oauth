package com.secuera.saml2auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Saml2OauthApplication 
{
    public static void main( String[] args )
    {
        SpringApplication.run(Saml2OauthApplication.class, args);
    }
}
